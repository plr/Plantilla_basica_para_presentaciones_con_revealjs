# Plantilla_basica_para_presentaciones_con_revealjs

Se trata del código de una plantilla básica para presentaciones (diapositivas) con reveal.js

## Necesidad

A veces uso reveal.js para las presentaciones o diapositivas que utilizo en las clases. Reveal.js tienen un demo.html con muchas posibilidades. Trabajar con tantas posibilidad puede resultar complicado. Sería bueno tener una plantilla html más básica con solo lo más esencial.

## Solución

He preparado una [plantilla html](https://codeberg.org/plr/Plantilla_basica_para_presentaciones_con_revealjs/src/branch/main/plantillabasicaderevealjs.html) con lo básico. Añado comentarios en el código para explicar qué habría que cambiar o añadir. Las rutas de los archivos para cargar los CSS y el JS al que se llama desde el HTML están configurados para tener que crear una carpeta dentro de la raíz de reveal.js. Habría que seguir los siguientes pasos:

- Descargar reveal.js desde GitHub: https://github.com/hakimel/reveal.js o el ZIP directamente (https://github.com/hakimel/reveal.js/archive/refs/heads/master.zip)
- Descomprimirlo donde se desee
- En la carpeta raíz o /reveal.js crear una carpeta con el nombre que se quiera
- Añadir a esa carpeta la plantilla que he creado: [plantillabasicaderevealjs.html](https://codeberg.org/plr/Plantilla_basica_para_presentaciones_con_revealjs/src/branch/main/plantillabasicaderevealjs.html)

Todo el texto de las etiquetas H1, H2, a, img, p, etc., es totalmente modificable según se quiera. Se han añadido comentarios en el html para explicar con un buen grado de detalle cada aspecto.

## La plantilla publicada en la web como ejemplo

La plantila compartida se puede ver en vivo en [este enlace](https://pedrolr.es/docencia/plantilaplrrevealjs/mis_presentaciones/plantillabasicaderevealjs.html).